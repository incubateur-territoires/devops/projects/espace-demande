data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "landing" {
  id = 45912249
}
data "gitlab_project" "application" {
  id = 55950625
}
locals {
  gitlab_project_ids = {
    landing     = data.gitlab_project.landing.id
    application = data.gitlab_project.application.id
  }
}

data "gitlab_group" "group" {
  group_id = 67429694
}
