resource "random_password" "reviews_postgresql_password" {
  count   = var.gitlab_environment_scope == "*" ? 1 : 0
  special = false
  length  = 64
}
locals {
  db_url = !local.deploy_managed_db ? "" : "postgresql://${urlencode(scaleway_rdb_user.app[0].name)}:${urlencode(scaleway_rdb_user.app[0].password)}@${scaleway_rdb_instance.database[0].load_balancer[0].ip}:${scaleway_rdb_instance.database[0].load_balancer[0].port}/${scaleway_rdb_database.app[0].name}?sslmode=require"
}
