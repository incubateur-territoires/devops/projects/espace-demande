resource "scaleway_object_bucket" "backup_bucket_production" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-database-backups"
}

resource "scaleway_object_bucket" "backup_bucket_development" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-dev-database-backups"
}
