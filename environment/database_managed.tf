locals {
  deploy_managed_db = contains(["development", "demo", "production"], var.gitlab_environment_scope)
}

resource "scaleway_rdb_instance" "database" {
  count = local.deploy_managed_db ? 1 : 0
  name  = "${var.project_slug}-${var.gitlab_environment_scope}"

  engine            = "PostgreSQL-15"
  node_type         = "DB-POP2-2C-8G"
  is_ha_cluster     = var.gitlab_environment_scope == "production"
  volume_type       = "sbs_5k"
  volume_size_in_gb = 10

  disable_backup            = false
  backup_schedule_frequency = 24
  backup_schedule_retention = 30

  private_network {
    pn_id       = var.bastion_private_network_id
    enable_ipam = true
  }
  lifecycle {
    prevent_destroy = true
  }
}

resource "scaleway_rdb_database" "app" {
  count       = local.deploy_managed_db ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  name        = var.project_slug

  lifecycle {
    prevent_destroy = true
  }
}

resource "random_password" "database_app_password" {
  count  = local.deploy_managed_db ? 1 : 0
  length = 64
}
resource "scaleway_rdb_user" "app" {
  count       = local.deploy_managed_db ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  name        = var.project_slug
  password    = random_password.database_app_password[0].result
}
resource "scaleway_rdb_privilege" "app" {
  count         = local.deploy_managed_db ? 1 : 0
  instance_id   = scaleway_rdb_instance.database[0].id
  user_name     = scaleway_rdb_user.app[0].name
  database_name = scaleway_rdb_database.app[0].name
  permission    = "all"
}

resource "scaleway_rdb_acl" "app" {
  count       = local.deploy_managed_db ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  acl_rules {
    description = "K8S public gateway IP"
    ip          = "${var.k8s_public_gw_ip}/32"
  }
  acl_rules {
    description = "Scaleway project bastion"
    ip          = "${var.bastion_ip}/32"
  }
}

resource "random_password" "database_metabase_password" {
  count  = local.deploy_managed_db && var.gitlab_environment_scope == "production" ? 1 : 0
  length = 64
}
resource "scaleway_rdb_user" "metabase" {
  count       = local.deploy_managed_db && var.gitlab_environment_scope == "production" ? 1 : 0
  instance_id = scaleway_rdb_instance.database[0].id
  name        = "${var.project_slug}-metabase"
  password    = random_password.database_metabase_password[0].result
}
# we should find a way to give less privileges for metabase, but for now scaleway only has this option
resource "scaleway_rdb_privilege" "metabase" {
  count         = local.deploy_managed_db && var.gitlab_environment_scope == "production" ? 1 : 0
  instance_id   = scaleway_rdb_instance.database[0].id
  user_name     = scaleway_rdb_user.metabase[0].name
  database_name = scaleway_rdb_database.app[0].name
  permission    = "custom"
}

resource "kubernetes_config_map_v1" "tunnel_command" {
  count = local.deploy_managed_db ? 1 : 0
  metadata {
    namespace = module.namespace.namespace
    name      = "doc-tunnel-command"
  }
  data = {
    command = "ssh -N -4 -p ${var.bastion_port} -L <local port>:${scaleway_rdb_instance.database[0].private_network[0].ip}:${scaleway_rdb_instance.database[0].private_network[0].port} bastion@${var.bastion_ip} # for ${var.gitlab_environment_scope}"
  }
}
