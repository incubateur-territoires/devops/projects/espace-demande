module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-reviews"
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key

  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  bastion_ip                 = scaleway_vpc_public_gateway_ip.bastion.address
  bastion_port               = scaleway_vpc_public_gateway.bastion.bastion_port

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.development_secret_org_id.result
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.dev_base_domain
  namespace                = "${var.project_slug}-development"
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key

  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  bastion_ip                 = scaleway_vpc_public_gateway_ip.bastion.address
  bastion_port               = scaleway_vpc_public_gateway.bastion.bastion_port

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.development_secret_org_id.result

  k8s_public_gw_ip = var.k8s_development_public_gw_ip
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "demo" {
  source                   = "./environment"
  gitlab_environment_scope = "demo"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base_domain              = var.demo_base_domain
  namespace                = "${var.project_slug}-demo"
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key

  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  bastion_ip                 = scaleway_vpc_public_gateway_ip.bastion.address
  bastion_port               = scaleway_vpc_public_gateway.bastion.bastion_port

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.development_secret_org_id.result

  k8s_public_gw_ip = var.k8s_development_public_gw_ip
  providers = {
    scaleway = scaleway.scaleway_project
  }
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base_domain              = var.prod_base_domain
  landing_page_domain      = var.landing_page_domain
  namespace                = var.project_slug
  gitlab_project_ids       = local.gitlab_project_ids
  project_slug             = var.project_slug
  project_name             = var.project_name
  scaleway_access_key      = var.scaleway_project_config.access_key
  scaleway_secret_key      = var.scaleway_project_config.secret_key

  bastion_private_network_id = scaleway_vpc_private_network.bastion.id
  bastion_ip                 = scaleway_vpc_public_gateway_ip.bastion.address
  bastion_port               = scaleway_vpc_public_gateway.bastion.bastion_port

  namespace_quota_max_cpu_requests  = 4
  namespace_quota_max_memory_limits = "20Gi"

  monitoring_org_id = random_string.production_secret_org_id.result

  k8s_public_gw_ip = var.k8s_production_public_gw_ip
  providers = {
    scaleway = scaleway.scaleway_project
  }
}
