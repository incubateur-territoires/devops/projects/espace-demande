variable "base_domain" {
  type = string
}

variable "landing_page_domain" {
  type    = string
  default = null
}
locals {
  landing_page_domain = var.landing_page_domain != null ? var.landing_page_domain : "landing.${var.base_domain}"
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}
locals {
  environment_slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

variable "namespace" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    landing     = number
    application = number
  })
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}

variable "namespace_quota_max_cpu_requests" {
  type    = string
  default = "2"
}

variable "namespace_quota_max_memory_limits" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}

variable "k8s_public_gw_ip" {
  type    = string
  default = null
}

variable "bastion_private_network_id" {
  type = string
}
variable "bastion_port" {
  type    = number
  default = null
}
variable "bastion_ip" {
  type    = string
  default = null
}
