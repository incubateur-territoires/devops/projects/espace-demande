resource "scaleway_object_bucket" "uploads" {
  name = "${var.project_slug}-${local.environment_slug}-uploads"
}

resource "scaleway_object_bucket" "images" {
  name = "${var.project_slug}-${local.environment_slug}-images"
  cors_rule {
    allowed_methods = ["GET", "HEAD"]
    allowed_origins = ["*"]
    max_age_seconds = 604800
  }
}
