locals {
  postgresql_helm_values = var.gitlab_environment_scope != "*" ? "" : <<-EOT
  postgresql:
    image:
      repository: postgis/postgis
      tag: 14-3.2
    enabled: true
    envVars:
      POSTGRES_DB: esd
      POSTGRES_USER: esd
      POSTGRES_PASSWORD: "${random_password.reviews_postgresql_password[0].result}"
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 100m
    persistence:
      data:
        storageClass: scw-bssd # Do not retain reviews env volumes
  EOT
}
module "configure_repository_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.gitlab_project_ids.application]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base_domain
}

module "configure_landing_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.gitlab_project_ids.landing]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = local.landing_page_domain
}

resource "gitlab_project_variable" "landing_page_helm_values" {
  project           = var.gitlab_project_ids.landing
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"
  protected         = false

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  resources:
    limits:
      memory: 256Mi
    requests:
      cpu: 10m

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: haproxy

  service:
    targetPort: 3000

  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}

  probes:
    liveness:
      path: /
      initialDelaySeconds: 0
    readiness:
      path: /
      initialDelaySeconds: 0
  EOT
}

moved {
  from = module.configure_esd_project
  to   = module.configure_esd_project["47049788"]
}
module "configure_esd_project" {
  for_each          = toset([tostring(var.gitlab_project_ids.application)])
  source            = "gitlab.com/vigigloo/tf-modules-gitlab/projectcommonconfiguration"
  version           = "0.0.1"
  project_id        = each.key
  environment_scope = var.gitlab_environment_scope
  helm_values       = <<-EOT
  backend:
    resources:
      limits:
        memory: 1024Mi
      requests:
        cpu: 100m

    ingress:
      enabled: true
      path: /api/
      annotations:
        kubernetes.io/ingress.class: haproxy-bis
        acme.cert-manager.io/http01-edit-in-place: null
      className: haproxy-bis
      certManagerClusterIssuer: letsencrypt-prod-dns01

    service:
      port: 80
      targetPort: 80

    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}

    envVars:
      APP_ENV: prod
      JWT_SECRET_KEY_BASE64:
        secretKeyRef:
          name: ${kubernetes_secret_v1.esd_jwt_secret.metadata[0].name}
          key: private
      JWT_PUBLIC_KEY_BASE64:
        secretKeyRef:
          name: ${kubernetes_secret_v1.esd_jwt_secret.metadata[0].name}
          key: public
      DATABASE_URL:
        secretKeyRef:
          name: esd-db
          key: uri
      # todo: correctly set these environment variables below
      CORS_ALLOW_ORIGIN: ""
      ELASTICSEARCH_URL: ""
      ELASTICSEARCH_HOST: ""
      ELASTICSEARCH_PORT: ""
      ELASTICSEARCH_SCHEME: ""
      ELASTICSEARCH_INDEX_PREFIX: ""
      PDF_GENERATION_ASSETS_BASE_PATH: ""
      CLIENT_BASE_URL: ""
      ROUTER_REQUEST_CONTEXT_HOST: ""
      ROUTER_REQUEST_CONTEXT_SCHEME: ""
      SALTO_BASE_URI: http://saltobaseuri
      SALTO_IDENTITY_BASE_URI: http://saltoidentitybaseuri
      SALTO_CLIENT_ID: ""
      SALTO_CLIENT_SECRET: ""
      SALTO_USERNAME: ""
      SALTO_PASSWORD: ""
      HTTP_CLIENT_PROXY: ""
      LYRA_BASE_URI: http://lyrabaseuri
      LYRA_ENCODED_CREDENTIALS: ""
      LYRA_MARKETPLACE_ID: ""
      MESSENGER_TRANSPORT_DSN: doctrine://default
    jobs:
      hooks:
        postInstall:
          - args: ["./bin/console.php", "doctrine:migrations:migrate", "latest", "--no-interaction"]
        preUpgrade:
          - args: ["./bin/console.php", "doctrine:migrations:migrate", "latest", "--no-interaction"]

    nginx:
      resources:
        limits:
          memory: 512Mi
        requests:
          cpu: 50m
      confPath: /etc/nginx/templates/default.conf.template
      envVars:
        NGINX_PORT: 80
        PHP_HOST: localhost
        PHP_PORT: 9000

    probes:
      liveness:
        ${var.gitlab_environment_scope == "*" ? "tcpSocket: {port: 80}" : "path: /"}
        initialDelaySeconds: 0
      readiness:
        ${var.gitlab_environment_scope == "*" ? "tcpSocket: {port: 80}" : "path: /"}
        initialDelaySeconds: 0

  frontend:
    resources:
      limits:
        memory: 256Mi
      requests:
        cpu: 10m

    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy-bis
        acme.cert-manager.io/http01-edit-in-place: null
      className: haproxy-bis
      certManagerClusterIssuer: letsencrypt-prod-dns01

    service:
      targetPort: 3000

    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}

    probes:
      liveness:
        initialDelaySeconds: 0
      readiness:
        initialDelaySeconds: 0

  ${local.postgresql_helm_values}
  EOT
}

resource "random_password" "app_secret" {
  length  = 32
  special = false
}
resource "gitlab_project_variable" "esd_app_secret_new" {
  environment_scope = var.gitlab_environment_scope
  project           = var.gitlab_project_ids.application
  key               = "HELM_BACKEND_ENV_VAR_APP_SECRET"
  value             = random_password.app_secret.result
}

resource "tls_private_key" "esd_jwt_private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "kubernetes_secret_v1" "esd_jwt_secret" {
  metadata {
    namespace = module.namespace.namespace
    name      = "esd-jwt-secret"
  }
  data = {
    private = base64encode(tls_private_key.esd_jwt_private_key.private_key_pem)
    public  = base64encode(tls_private_key.esd_jwt_private_key.public_key_pem)
  }
}
resource "random_password" "jwt_passphrase" {
  length  = 64
  special = false
}
resource "gitlab_project_variable" "esd_jwt_passphrase_new" {
  environment_scope = var.gitlab_environment_scope
  project           = var.gitlab_project_ids.application
  key               = "HELM_BACKEND_ENV_VAR_JWT_PASSPHRASE"
  value             = random_password.jwt_passphrase.result
}

# only required on review env because the connection string is generated by the CI for each branch
resource "gitlab_project_variable" "postgresql_user_new" {
  count             = var.gitlab_environment_scope == "*" ? 1 : 0
  project           = var.gitlab_project_ids.application
  environment_scope = var.gitlab_environment_scope
  key               = "REVIEWS_POSTGRESQL_USER"
  value             = "esd"
}
resource "gitlab_project_variable" "postgresql_password_new" {
  count             = var.gitlab_environment_scope == "*" ? 1 : 0
  project           = var.gitlab_project_ids.application
  environment_scope = var.gitlab_environment_scope
  key               = "REVIEWS_POSTGRESQL_PASSWORD_URLENC"
  value             = urlencode(random_password.reviews_postgresql_password[0].result)
}
resource "gitlab_project_variable" "postgresql_db_new" {
  count             = var.gitlab_environment_scope == "*" ? 1 : 0
  project           = var.gitlab_project_ids.application
  environment_scope = var.gitlab_environment_scope
  key               = "REVIEWS_POSTGRESQL_DB"
  value             = "esd"
}

resource "kubernetes_secret_v1" "esd_db_config" {
  metadata {
    namespace = module.namespace.namespace
    name      = "esd-db"
  }
  data = {
    uri = local.db_url
  }
}

resource "gitlab_project_variable" "s3_config_new" {
  for_each = {
    "S3_BUCKET"     = scaleway_object_bucket.uploads.name
    "S3_ENDPOINT"   = "https://s3.fr-par.scw.cloud"
    "S3_REGION"     = scaleway_object_bucket.uploads.region
    "S3_ACCESS_KEY" = var.scaleway_access_key
    "S3_SECRET_KEY" = var.scaleway_secret_key
  }
  project           = var.gitlab_project_ids.application
  environment_scope = var.gitlab_environment_scope
  key               = "HELM_BACKEND_ENV_VAR_${each.key}"
  value             = each.value
}

resource "gitlab_project_variable" "s3_config_image" {
  for_each = {
    "S3_IMAGE_BUCKET"     = scaleway_object_bucket.images.name
    "S3_IMAGE_ENDPOINT"   = "https://s3.fr-par.scw.cloud"
    "S3_IMAGE_REGION"     = scaleway_object_bucket.images.region
    "S3_IMAGE_ACCESS_KEY" = var.scaleway_access_key
    "S3_IMAGE_SECRET_KEY" = var.scaleway_secret_key
  }
  project           = var.gitlab_project_ids.application
  environment_scope = var.gitlab_environment_scope
  key               = "HELM_BACKEND_ENV_VAR_${each.key}"
  value             = each.value
}
