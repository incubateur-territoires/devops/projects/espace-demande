resource "gitlab_deploy_token" "dependency_proxy" {
  name   = "Used for authenticating with group level dependency proxy (managed by terraform)"
  group  = data.gitlab_group.group.id
  scopes = ["read_registry", "write_registry"]
}

resource "gitlab_project_variable" "dependency_proxy_auth" {
  for_each = toset([local.gitlab_project_ids.application])
  project  = each.key
  key      = "DOCKER_AUTH_CONFIG"
  value = jsonencode(
    {
      auths = {
        "gitlab.com" = {
          auth = gitlab_deploy_token.dependency_proxy.token
        }
      }
    }
  )
}
